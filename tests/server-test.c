/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * server-test.c: Standalone server test.
 *
 * Authors:
 *      Alex Graveley (alex@ximian.com)
 *
 * Copyright (C) 2001, Ximian, Inc.
 */

#include <glib.h>
#include <string.h>

#include <libsoup/soup.h>

gchar payload [] = "Hello World!";

static void 
hello_callback (SoupServerContext *context, 
		SoupMessage       *msg, 
		gpointer           user_data)
{
	soup_message_set_error (msg, SOUP_ERROR_OK);

	g_print ("Request from user %s\n", 
		 soup_server_auth_get_user (context->auth));
	g_print ("Password Check: %s\n\n", 
		 soup_server_auth_check_passwd (context->auth, "foo") ?
		 "PASSED" :
		 "FAILED");

	msg->response.owner = SOUP_BUFFER_STATIC;
	msg->response.body = payload;
	msg->response.length = strlen (payload);
}

static void
append_header (gchar *key, gchar *value, GString *body)
{
	g_string_sprintfa (body, "%s: %s\n", key, value);
}

static void 
default_callback (SoupServerContext *context, 
		  SoupMessage       *msg, 
		  gpointer           user_data)
{
	GString *body;
	gchar *uri;

	soup_message_set_error_full (msg, SOUP_ERROR_OK, "All Is Well");

	soup_message_add_header (msg->response_headers, 
				 "X-TestMulti", 
				 "I like cheese");

	soup_message_add_header (msg->response_headers, 
				 "X-TestMulti", 
				 "Especially Edam");

	soup_message_add_header (msg->response_headers, 
				 "This-Is-A-Test", 
				 "The rain in spain...");

	body = g_string_new (NULL);

	uri = soup_uri_to_string (soup_context_get_uri (msg->context), TRUE);
	g_string_sprintfa (body, 
			   "Requested Uri: %s\n\n", 
			   uri);
	g_free (uri);

	g_string_sprintfa (body, "Request Headers:\n");
	soup_message_foreach_header (msg->request_headers, 
				     (GHFunc) append_header, 
				     body);

	g_string_sprintfa (body, 
			   "\nRequest Body:\n----\n%s\n----\n", 
			   msg->request.body);

	msg->response.owner = SOUP_BUFFER_SYSTEM_OWNED;
	msg->response.body = body->str;
	msg->response.length = body->len;

	g_string_free (body, FALSE);
}

static gboolean 
push_data3 (SoupServerMessage *smsg)
{
	soup_server_message_finish (smsg);
	return FALSE;
}

static gboolean
push_data2 (SoupServerMessage *smsg)
{
	SoupMessage *msg = soup_server_message_get_source (smsg);
	GString *body;
	gchar *uri;

	body = g_string_new (NULL);

	uri = soup_uri_to_string (soup_context_get_uri (msg->context), TRUE);
	g_string_sprintfa (body, 
			   "Requested Uri: %s\n\n", 
			   uri);
	g_free (uri);

	g_string_sprintfa (body, "Request Headers:\n");
	soup_message_foreach_header (msg->request_headers, 
				     (GHFunc) append_header, 
				     body);

	g_string_sprintfa (body, 
			   "\nRequest Body:\n----\n%s\n----\n", 
			   msg->request.body);

	soup_server_message_add_data (smsg, 
				      SOUP_BUFFER_SYSTEM_OWNED, 
				      body->str,
				      body->len);

	g_string_free (body, FALSE);

	g_timeout_add (4000, (GSourceFunc) push_data3, smsg);

	return FALSE;
}

static gboolean 
push_data1 (SoupServerMessage *smsg)
{
	SoupMessage *msg = soup_server_message_get_source (smsg);

	soup_message_set_error_full (msg, SOUP_ERROR_OK, "All Is Well");

	soup_message_add_header (msg->response_headers, 
				 "X-TestMulti", 
				 "I like cheese");

	soup_message_add_header (msg->response_headers, 
				 "X-TestMulti", 
				 "Especially Edam");

	soup_message_add_header (msg->response_headers, 
				 "This-Is-A-Test", 
				 "The rain in spain...");

	soup_server_message_start (smsg);

	g_timeout_add (4000, (GSourceFunc) push_data2, smsg);

	return FALSE;
}

static void
push_callback (SoupServerContext *context, 
	       SoupMessage       *msg, 
	       gpointer           user_data)
{
	SoupServerMessage *smsg;

	smsg = soup_server_message_new (msg);

	g_timeout_add (4000, (GSourceFunc) push_data1, smsg);
}

static gboolean 
auth_callback (SoupServerAuthContext *auth_ctx,
	       SoupServerAuth        *auth,
	       SoupMessage           *msg, 
	       gpointer               data)
{
	if (!auth) {
		g_print ("No auth, failing\n\n");
		return FALSE;
	}

	g_print ("Request from user %s\n", 
		 soup_server_auth_get_user (auth));
	g_print ("Password Check: %s\n\n", 
		 soup_server_auth_check_passwd (auth, "foo") ?
		 "PASSED" :
		 "FAILED");

	return soup_server_auth_check_passwd (auth, "foo");
}

void
soup_server_init (SoupServer *server)
{
	SoupServerAuthContext auth_ctx = { 0 };

	auth_ctx.types = SOUP_AUTH_TYPE_BASIC | SOUP_AUTH_TYPE_DIGEST;
	auth_ctx.callback = auth_callback;
	auth_ctx.basic_info.realm = "Hell";
	auth_ctx.digest_info.realm = "My Butt";

	/*
	 * Register a handler for requests for "/hello"
	 */
	soup_server_register (server, 
			      "/hello", 
			      &auth_ctx, 
			      hello_callback, 
			      NULL, 
			      NULL);

	/*
	 * Register a handler for all other request paths
	 */
	soup_server_register (server, 
			      NULL, 
			      NULL, 
			      default_callback, 
			      NULL, 
			      NULL);

	/*
	 * Register a handler for delayed server push
	 */
	soup_server_register (server, 
			      "/push", 
			      NULL, 
			      push_callback, 
			      NULL, 
			      NULL);
}

int
main (int argc, char **argv)
{
	SoupServer *serv;

	serv = soup_server_new (SOUP_PROTOCOL_HTTP, SOUP_SERVER_ANY_PORT);

	g_print ("Starting server on port %d\n", soup_server_get_port (serv));
	g_print ("Waiting for requests...\n");

	soup_server_init (serv);

	soup_server_run (serv);

	return 0;
}
