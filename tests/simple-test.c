/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * simple-test.c: Test single message sending.
 *
 * Authors:
 *      Alex Graveley (alex@helixcode.com)
 *
 * Copyright (C) 2000, Helix Code, Inc.
 */

#include <string.h>
#include <stdlib.h>

#include <libsoup/soup.h>

GMainLoop *loop;

static SoupHandlerResult
chunk_handler (SoupMessage *msg, gpointer user_data)
{
	static guint last_size = 0;

	g_print ("\nChunk Handler\n");
	g_print ("\tChunk Size: %d\n", msg->response.length - last_size);

	last_size = msg->response.length;

	return SOUP_HANDLER_CONTINUE;
}

static void 
find_header_size (gchar *key, gchar *val, guint *pad)
{
	*pad = MAX (strlen (key), *pad);
}

static void 
list_headers (gchar *key, GSList *vals, guint *pad)
{
	guint len = 80 - 6 - *pad;

	while (vals) {
		gchar *valt = g_strndup (vals->data, len);
		g_print ("    %*s: %s\n", *pad, key, valt);
		g_free (valt);

		vals = vals->next;
	}
}

static void
message_finished (SoupMessage *msg, gpointer user_data)
{
	guint pad = 0;

	if (SOUP_MESSAGE_IS_ERROR (msg))
		g_print ("\nMessage failed: %d \"%s\"\n",
			 msg->errorcode,
			 msg->errorphrase);
	else {
		g_print ("\n\nResponse Finished\n\n");
		
		g_print ("Response Status Code: %d \"%s\"\n", 
			 msg->errorcode,
			 msg->errorphrase);
		
		g_print ("Response Headers:\n");
		
		g_hash_table_foreach (msg->response_headers, 
				      (GHFunc) find_header_size,
				      &pad);

		g_hash_table_foreach (msg->response_headers, 
				      (GHFunc) list_headers,
				      &pad);
	}

	g_print ("\nResponse Length: %d\n", msg->response.length);

	if (msg->response.length) {
		g_print ("\nPress RETURN to view response.\n"); 

		getchar ();

		g_print ("---\n%s\n---\n", msg->response.body);
	}

	g_main_quit (loop);
}

int 
main (int argc, char **argv)
{
	SoupContext *context;
	SoupMessage *message;
	gchar *uri;

	if (argc < 2) {
		g_print ("Usage: simple-test url [http-method] [http-body]\n");
		g_print ("Using http://www.ximian.com instead...\n");
		argv [1] = "http://www.ximian.com";
	}

	loop = g_main_new (FALSE);

	context = soup_context_get (argv [1]);

	if (!context) {
		g_print ("Invalid URL: %s\n", argv [1]);
		return 1;
	}

	uri = soup_uri_to_string (soup_context_get_uri (context), TRUE);

	g_print ("Creating new message for: %s\n", uri);
	g_free (uri);

	if (argc > 3) {
		message = soup_message_new_full (context, 
						 argv [2], 
						 SOUP_BUFFER_STATIC,
						 argv [3],
						 strlen (argv [3]));
	} else if (argc > 2)
		message = soup_message_new (context, argv [2]);
        else 
		message = soup_message_new (context, SOUP_METHOD_GET);

	soup_context_unref (context);

	soup_message_add_handler (message,
				  SOUP_HANDLER_DATA,
				  NULL,
				  chunk_handler,
				  NULL);

	g_print ("Queueing message.\n");
	soup_message_queue (message, message_finished, NULL);

	g_print ("Running main loop.\n");
	g_main_run (loop);
	
	return 0;
}
