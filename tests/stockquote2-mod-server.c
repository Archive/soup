#include <glib.h>
#include <stdio.h>

#include "stockquote2.h"

static void 
callback (SoupEnv *env, guchar *name, gfloat *ret, gpointer unused)
{
	*ret = 1.245;
}

void
soup_server_init (SoupServer *server)
{
	StockQuote_handlers meths = {
		callback
	};

	StockQuote_register (server, NULL, &meths, NULL);
}
