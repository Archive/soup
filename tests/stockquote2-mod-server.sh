#!/bin/sh

if [ -f ../src/soup-httpd/soup-httpd ]; then
	../src/soup-httpd/soup-httpd .libs/libstockquote2.so
else
	cd ../src/soup-httpd && 				\
	make && 						\
	echo -e "\n\nRunning soup-httpd...\n\n" && 		\
	./soup-httpd ../../tests/.libs/libstockquote2.so
fi;
