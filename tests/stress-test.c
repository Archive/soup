/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * stress-test.c: Test multiple simultaneous messages.
 *
 * Authors:
 *      Alex Graveley (alex@helixcode.com)
 *
 * soup_base64_encode() written by Joe Orton, borrowed from ghttp.
 *
 * Copyright (C) 2000, Helix Code, Inc.
 */

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <libsoup/soup.h>

gint message_cnt = 3;
gint per_message = 120;
gint iterations = 2;

gboolean send_body = FALSE;
gboolean replace_each = FALSE;

GMainLoop *loop;
SoupContext *context;
GSList *mlist = NULL;

typedef struct {
	SoupMessage *msg;
	int name;
	int cnt;
} MessageInfo;

char request_body[] =                       \
	"<SOAP-ENV:Envelope>"               \
	"  <SOAP-ENV:Body>"                 \
	"    <m:GetCurrentTemperature>"     \
	"      <city>Boston</city>"         \
	"      <country>US</country>"       \
	"    </m:GetCurrentTemperature>"    \
	"  </SOAP-ENV:Body>"                \
	"</SOAP-ENV:Envelope>";

static void message_finished (SoupMessage *msg, gpointer user_data);

static SoupHandlerResult
body_handler (SoupMessage *msg, gpointer user_data)
{
	if (!soup_connection_is_keep_alive (msg->connection))
		g_print ("Closing connection: %p\n", msg->connection);

	return SOUP_HANDLER_CONTINUE;
}

static void
make_message (MessageInfo *info)
{
	if (send_body)
		info->msg = 
			soup_message_new_full (context,
					       SOUP_METHOD_POST,
					       SOUP_BUFFER_STATIC,
					       request_body,
					       strlen (request_body));
	else
		info->msg = soup_message_new (context, SOUP_METHOD_GET);

	soup_message_add_handler (info->msg,
				  SOUP_HANDLER_FINISHED,
				  NULL,
				  body_handler,
				  info);
	
	soup_message_queue (info->msg, message_finished, info);
}

static void
create_messages (void)
{
	gint i = 0;
	MessageInfo *info;

	for (i = 0; i < message_cnt; i++) {
		info = g_new0 (MessageInfo, 1);
		info->name = 'A' + i;

		make_message (info);

		mlist = g_slist_prepend (mlist, info);
	}
}

static void
message_finished (SoupMessage *msg, gpointer user_data)
{
	MessageInfo *info = user_data;

	if (SOUP_MESSAGE_IS_ERROR (msg)) 
		g_print ("\t*%c* %3d status:%d \"%s\" length:%d\n",
			 info->name,
			 info->cnt,
			 msg->errorcode,
			 msg->errorphrase,
			 msg->response.length);

	if (info->cnt++ < per_message) {
		if (replace_each)
			make_message (info);
		else
			soup_message_queue (msg, message_finished, info);
	} else {
		g_print ("\t*%c* finished\n", info->name);

		mlist = g_slist_remove (mlist, info);
		if (!mlist) {
			g_print ("\nDone with all active messages.\n");
			g_main_quit (loop);
		}
	}
}

int 
main (int argc, char **argv)
{
	gchar *uri;

	if (argc < 2) {
		g_print ("Usage: simple-test url [msgcnt] [permsg] "
			 "[iterations] [postdata] [replace]\n");
		g_print ("Using http://www.ximian.com instead...\n");
		argv [1] = "http://www.ximian.com";
	}

	loop = g_main_new (FALSE);

	context = soup_context_get (argv [1]);
	if (!context) {
		g_print ("Invalid URL: %s\n", argv [1]);
		return 1;
	}

	if (argc > 2) message_cnt = atoi (argv [2]);
	if (argc > 3) per_message = atoi (argv [3]);
	if (argc > 4) iterations = atoi (argv [4]);
	if (argc > 5) replace_each = TRUE;
	if (argc > 6) send_body = TRUE;

	uri = soup_uri_to_string (soup_context_get_uri (context), TRUE);
	g_print ("Creating new messages for: %s\n", uri);
	g_free (uri);

	g_print ("Queueing messages.\n");
	create_messages ();

	g_print ("Running main loop.\n\n");
	g_main_run (loop);
	
	while (--iterations) {
		gint sleep_cnt = 0;

		g_print ("Going to sleep");

		while (sleep_cnt++ < 20) {
			sleep (1);
			g_print (".");
		}
		
		g_print ("\n\nRequeueing messages.\n");
		create_messages ();

		g_print ("Running main loop.\n\n");
		g_main_run (loop);
	}

	g_print ("Exiting");

	return 0;
}
