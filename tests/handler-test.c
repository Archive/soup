/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * handler-test.c: Test message handlers.
 *
 * Authors:
 *      Alex Graveley (alex@helixcode.com)
 *
 * soup_base64_encode() written by Joe Orton, borrowed from ghttp.
 *
 * Copyright (C) 2000, Helix Code, Inc.
 */

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <libsoup/soup.h>

GMainLoop *loop;

static SoupHandlerResult
transport_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Transport Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
informational_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Informational Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
success_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Success Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
redirect_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Redirect Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");

	g_print ("\tRedirecting to: %s\n", 
		 soup_message_get_header (msg->response_headers, "Location"));
	g_print ("\n\t*NOTE* No CHUNK or POST_BODY handlers will be run until\n"
		 "\tafter fetching the URL we are being redirected to.\n"
		 "\tSee comments in source.\n");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
client_error_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Client Error Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
server_error_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Server Error Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
unknown_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Unknown Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
handler_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Handler Handler\n", 
		 user_data ? "POST-BODY" : "PRE-BODY");
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
chunk_handler (SoupMessage *msg, gpointer user_data)
{
	static guint last_size = 0;

	g_print ("\nChunk Handler\n");
	g_print ("\tChunk Size: %d\n", msg->response.length - last_size);

	last_size = msg->response.length;
	return SOUP_HANDLER_CONTINUE;
}

static SoupHandlerResult
body_handler (SoupMessage *msg, gpointer user_data)
{
	g_print ("\n%s Body Handler\n", user_data ? "POST-BODY" : "PRE-BODY");
	g_print ("\tStatus Code: %d \"%s\"\n", 
		 msg->errorcode, 
		 msg->errorphrase);
	g_print ("\tBody Length: %d\n", msg->response.length);
	return SOUP_HANDLER_CONTINUE;
}

static void 
find_header_size (gchar *key, gchar *val, guint *pad)
{
	*pad = MAX (strlen (key), *pad);
}

static void 
list_headers (gchar *key, GSList *vals, guint *pad)
{
	guint len = 80 - 6 - *pad;

	while (vals) {
		gchar *valt = g_strndup (vals->data, len);
		g_print ("    %*s: %s\n", *pad, key, valt);
		g_free (valt);

		vals = vals->next;
	}
}

static void
message_finished (SoupMessage *msg, gpointer user_data)
{
	guint pad = 0;

	if (SOUP_MESSAGE_IS_ERROR (msg))
		g_print ("\nMessage failed: %d \"%s\"\n",
			 msg->errorcode,
			 msg->errorphrase);
	else {
		g_print ("\n\nResponse Finished\n\n");
		
		g_print ("Response Status Code: %d \"%s\"\n", 
			 msg->errorcode,
			 msg->errorphrase);
		
		g_print ("Response Headers:\n");
		
		g_hash_table_foreach (msg->response_headers, 
				      (GHFunc) find_header_size,
				      &pad);

		g_hash_table_foreach (msg->response_headers, 
				      (GHFunc) list_headers,
				      &pad);
	}

	g_print ("\nResponse Length: %d\n", msg->response.length);

	g_main_quit (loop);
}

int 
main (int argc, char **argv)
{
	SoupContext *context;
	SoupMessage *message;
	SoupHandlerFilter filt;
	gchar *uri;

	if (argc < 2) {
		g_print ("Usage: simple-test url\n");
		g_print ("Using http://www.ximian.com instead...\n");
		argv [1] = "http://www.ximian.com";
	}

	loop = g_main_new (FALSE);

	context = soup_context_get (argv [1]);

	if (!context) {
		g_print ("Invalid URL: %s\n", argv [1]);
		return 1;
	}

	uri = soup_uri_to_string (soup_context_get_uri (context), TRUE);

	g_print ("Creating new message for: %s\n", uri);
	g_free (uri);

	message = soup_message_new (context, SOUP_METHOD_GET);
	soup_context_unref (context);

	g_print ("Adding Message Handlers\n");

	filt.type = SOUP_FILTER_ERROR_CLASS;
	filt.data.errorclass = SOUP_ERROR_CLASS_TRANSPORT;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  transport_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  transport_handler,
				  &loop);

	filt.data.errorclass = SOUP_ERROR_CLASS_INFORMATIONAL;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  informational_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  informational_handler,
				  &loop);

	filt.data.errorclass = SOUP_ERROR_CLASS_REDIRECT;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  redirect_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  redirect_handler,
				  &loop);

	filt.data.errorclass = SOUP_ERROR_CLASS_SUCCESS;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  success_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  success_handler,
				  &loop);

	filt.data.errorclass = SOUP_ERROR_CLASS_CLIENT_ERROR;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  client_error_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  client_error_handler,
				  &loop);

	filt.data.errorclass = SOUP_ERROR_CLASS_SERVER_ERROR;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  server_error_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  server_error_handler,
				  &loop);

	filt.data.errorclass = SOUP_ERROR_CLASS_UNKNOWN;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  unknown_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  unknown_handler,
				  &loop);

	filt.data.errorclass = SOUP_ERROR_CLASS_HANDLER;

	soup_message_add_handler (message,
				  SOUP_HANDLER_HEADERS,
				  &filt,
				  handler_handler,
				  NULL);
	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  &filt,
				  handler_handler,
				  &loop);

	soup_message_add_handler (message,
				  SOUP_HANDLER_FINISHED,
				  NULL,
				  body_handler,
				  &loop);

	soup_message_add_handler (message,
				  SOUP_HANDLER_DATA,
				  NULL,
				  chunk_handler,
				  NULL);

	/* 
	 * Only PRE_BODY handlers will run for redirected URLS, as the message
	 * is requeued to the new URL before reading the body.  To receive the
	 * body of the redirect itself, you must inhibit the internal Soup
	 * redirect handler: 
	 */
	 /* soup_message_set_flags (message, SOUP_MESSAGE_NO_REDIRECT); */

	g_print ("Queueing message.\n");
	soup_message_queue (message, message_finished, NULL);

	g_print ("Running main loop.\n");
	g_main_run (loop);
	
	return 0;
}
