
#include <glib.h>
#include <stdio.h>

#include "stockquote2.h"

GMainLoop *loop = NULL;

static void 
callback (SoupEnv      *env, 
	  gfloat        body, 
	  gpointer      user_data)
{
	if (SOUP_ENV_IS_FAULT (env))
		g_print ("Fault!\n");
	else if (SOUP_ENV_IS_TRANSPORT_ERROR (env))
		g_print ("Transport Error!\n");
	else
		g_print ("Async Response: %f\n\n", body);

	g_print ("Quiting...\n");
	g_main_quit (loop);
}

int 
main (int argc, char **argv)
{
	gfloat ret = 0;
	SoupEnv *env = soup_env_new ();
	loop = g_main_new (FALSE);

	g_print ("Sending Syncronously: %s\n", argv [1]);
	StockQuote_GetLastTradePrice (env, argv [1], &ret);

	if (SOUP_ENV_IS_FAULT (env))
		g_print ("Fault!\n");
	else if (SOUP_ENV_IS_TRANSPORT_ERROR (env))
		g_print ("Transport Error!\n");
	else
		g_print ("Response: %f\n\n", ret);

	g_print ("Sending Asyncronously: %s\n", argv [1]);
	StockQuote_GetLastTradePrice_async (env, argv [1], callback, NULL);
	g_print ("Freeing Environment\n");
	soup_env_free (env);

	g_print("Looping\n");
	g_main_run(loop);

	return 0;
}
