/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * filesys-server.c: Standalone DAV filesystem server test.
 *
 * Authors:
 *      Alex Graveley (alex@ximian.com)
 *
 * Copyright (C) 2001, Ximian, Inc.
 */

#include <glib.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <libsoup/soup.h>

gchar *prefix = ".";

SoupDavServerHandlers handlers;

static void
filesys_handler (SoupServerContext *ctx, SoupMessage *msg, gpointer user_data)
{
	g_print ("Request for: %s\n", ctx->path);
	soup_dav_server_process (ctx, &handlers, NULL);
}

int
main (int argc, char **argv)
{
	SoupServer *serv;
	gchar *wd;

	serv = soup_server_new (SOUP_PROTOCOL_HTTP, 8888);

	if (argc > 1)
		wd = prefix = argv [1];
	else {
		wd = getenv ("PWD");
		if (wd)
			prefix = wd;
		else
			wd = "/";
	}

	g_print ("Starting DAV Filesystem server on port %d\n"
		 "For path \"%s\"\n\n",
		 soup_server_get_port (serv),
		 wd);

	g_print ("Waiting for requests...\n");

	soup_server_register (serv, NULL, NULL, filesys_handler, NULL, NULL);
	soup_server_run (serv);

	return 0;
}

/*
 * Server handler implementation
 */
static gboolean  
uri_exists (SoupServerContext    *ctx, 
	    const gchar          *path,
	    gpointer              user_data)
{
	gboolean ret = TRUE;
	gchar *mypath;
	struct stat file_stat;

	mypath = g_strconcat (prefix, "/", path, NULL);

	g_print ("statting %s\n", mypath);

	if (stat (mypath, &file_stat) != 0)
		ret = FALSE;

	g_free (mypath);

	return ret;
}

static gboolean  
is_collection (SoupServerContext    *ctx, 
	       const gchar          *path,
	       gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *mypath;
	struct stat file_stat;

	mypath = g_strconcat (prefix, "/", path, NULL);

	if (stat (mypath, &file_stat) == 0 && S_ISDIR (file_stat.st_mode))
		ret = TRUE;

	g_free (mypath);

	return ret;	
}

static GSList *
opt_describe_locks (SoupServerContext    *ctx, 
		    const gchar          *path,
		    gpointer              user_data)
{
	return NULL;
}

static gboolean  
opt_lock (SoupServerContext    *ctx,  
	  const gchar          *path,
	  const SoupDavLock    *lock, 
	  gpointer              user_data)
{
	return TRUE;
}

static gboolean  
opt_unlock (SoupServerContext    *ctx,  
	    const gchar          *path,
	    const SoupDavLock    *lock, 
	    gpointer              user_data)
{
	return TRUE;
}

static gboolean  
create_collection (SoupServerContext    *ctx,
		   const gchar          *path,
		   gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);

	if (mkdir (mypath, 0777) == 0)
		ret = TRUE;
	else {
		switch (errno) {
		case EPERM:
		case EEXIST:
		case EFAULT:
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		case ENOSPC:
		default:
		}
	}

	g_free (mypath);

	return ret;
}

static gboolean  
create_doc (SoupServerContext    *ctx,
	    const gchar          *path,
	    const SoupDataBuffer *buf, 
	    gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);
	gint fd;

	fd = creat (mypath, 0777);
	if (fd < 0)
		switch (errno) {
		case EEXIST:
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		case ENOSPC:
		default:
		}
	else {
		if (write (fd, buf->body, buf->length) != buf->length) {
			switch (errno) {
			case EPIPE:
			case EAGAIN:
			case EINTR:
			case ENOSPC:
			case EIO:
			}
		} else
			ret = TRUE;
	}

	g_free (mypath);

	return ret;
}

static gboolean  
delete_collection (SoupServerContext    *ctx, 
		   const gchar          *path,
		   gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);

	if (rmdir (mypath) == 0)
		ret = TRUE;
	else {
		switch (errno) {
		case EPERM:
		case EFAULT:
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		case ENOTEMPTY:
		case EBUSY:
		default:
		}
	}

	g_free (mypath);

	return ret;
}

static gboolean  
delete_doc (SoupServerContext    *ctx, 
	    const gchar          *path,
	    gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);

	if (unlink (mypath) == 0)
		ret = TRUE;
	else {
		switch (errno) {
		case EPERM:
		case EFAULT:
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		case ENOTEMPTY:
		case EBUSY:
		case EISDIR:
		default:
		}
	}

	g_free (mypath);

	return ret;
}

static gboolean  
can_delete (SoupServerContext    *ctx, 
	    const gchar          *path,
	    gpointer              user_data)
{	
	return TRUE;
}

static GSList *
list_contents (SoupServerContext    *ctx, 
	       const gchar          *path,
	       gpointer              user_data)
{
	GSList *ret = NULL;
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);
	DIR *dp;
	struct dirent *de;

	dp = opendir (mypath);
	if (!opendir) {
		switch (errno) {
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		default:
		}
	}

	while ((de = readdir (dp))) {
		if (!strcmp (de->d_name, ".") || !strcmp (de->d_name, ".."))
			continue;

		ret = g_slist_prepend (ret, g_strdup (de->d_name));
	}

	closedir (dp);

	g_slist_reverse (ret);

	g_free (mypath);

	return ret;
}

static gboolean  
get_content (SoupServerContext    *ctx,  
	     const gchar          *path,
	     SoupDataBuffer       *out_buf, 
	     gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);
	FILE *f;
	guchar buf [8192];
	gint readlen;
	GByteArray *data;

	f = fopen (mypath, "r");
	if (!f) {
		switch (errno) {
		case EEXIST:
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		case ENOSPC:
		default:
		}

		goto FINISH;
	}

	data = g_byte_array_new ();

	while ((readlen = fread (buf, sizeof (buf), 1, f))) {
		g_byte_array_append (data, buf, readlen);
	}

	out_buf->body = data->data;
	out_buf->length = data->len;

	g_byte_array_free (data, FALSE);

 FINISH:
	g_free (mypath);

	return ret;
}

static gboolean  
set_content (SoupServerContext    *ctx,  
	     const gchar          *path,
	     SoupDataBuffer       *in_buf, 
	     gpointer              user_data)
{
	gboolean ret = FALSE;
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);
	FILE *f;

	f = fopen (mypath, "w");
	if (!f) {
		switch (errno) {
		case EEXIST:
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		case ENOSPC:
		default:
		}

		goto FINISH;
	}

	if (!fwrite (in_buf->body, in_buf->length, 1, f)) {
		if (ferror (f)) {
		}
	}	

 FINISH:
	g_free (mypath);

	return ret;
}

static gboolean  
get_dav_prop (SoupServerContext    *ctx,  
	      const gchar          *path,
	      SoupDavProp          *out_prop, 
	      gpointer              user_data)
{
	return TRUE;
}

static gboolean  
set_dav_prop (SoupServerContext    *ctx,  
	      const gchar          *path,
	      const SoupDavProp    *prop, 
	      gpointer              user_data)
{
	return TRUE;
}

static GSList *
list_custom_props (SoupServerContext    *ctx, 
		   const gchar          *path,
		   gpointer              user_data)
{
	return NULL;
}

static gboolean 
get_custom_prop (SoupServerContext    *ctx,  
		 const gchar          *path,
		 SoupDavProp          *out_prop, 
		 gpointer              user_data)
{
	return TRUE;
}

static gboolean  
set_custom_prop (SoupServerContext    *ctx,  
		 const gchar          *path,
		 const SoupDavProp    *prop, 
		 gpointer              user_data)
{
	return TRUE;
}

static gboolean 
delete_custom_prop (SoupServerContext    *ctx,  
		    const gchar          *path,
		    const SoupDavProp    *prop, 
		    gpointer              user_data)
{
	return TRUE;
}

static void 
opt_move (SoupServerContext    *ctx,
	  const gchar          *path,
	  const gchar          *new_path,
	  gboolean              overwrite,
	  gpointer              user_data)
{
	gchar *	mypath = g_strconcat (prefix, "/", path, NULL);
	gchar *	mynewpath = g_strconcat (prefix, "/", new_path, NULL);

	if (rename (mypath, mynewpath) != 0) {
		switch (errno) {
		case EXDEV:
		case EBUSY:
		case EINVAL:
		case EPERM:
		case EFAULT:
		case EACCES:
		case ENOENT:
		case ENOTDIR:
		case ENOTEMPTY:
		case EISDIR:
		default:
		}
	}

	g_free (mypath);
	g_free (mynewpath);
}

SoupDavServerHandlers handlers = {
	uri_exists,
	is_collection,
	opt_describe_locks,
	opt_lock,
	opt_unlock,
	create_collection,
	create_doc,
	delete_collection,
	delete_doc,
	can_delete,
	list_contents,
	get_content,
	set_content,
	get_dav_prop,
	set_dav_prop,
	list_custom_props,
	get_custom_prop,
	set_custom_prop,
	delete_custom_prop,
	opt_move,
	NULL,
};
