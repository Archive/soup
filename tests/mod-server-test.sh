#!/bin/sh

if [ -f ../src/soup-httpd/soup-httpd ]; then
	../src/soup-httpd/soup-httpd .libs/libmod-server-test.so
else
	cd ../src/soup-httpd && 				\
	make && 						\
	echo -e "\n\nRunning soup-httpd...\n\n" && 		\
	./soup-httpd ../../tests/.libs/libmod-server-test.so
fi;
