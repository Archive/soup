#include <glib.h>
#include <stdio.h>

#include "stockquote2.h"

static void 
callback (SoupEnv *env, guchar *name, gfloat *ret, gpointer unused)
{
	*ret = 1.245;
}

void
soup_server_init (SoupServer *server)
{
	StockQuote_handlers meths = {
		callback
	};

	StockQuote_register (server, NULL, &meths, NULL);
}

#ifndef SOUP_SERVER_MODULE
int 
main (int argc, char **argv)
{
	SoupServer *serv;

#ifdef SOUP_SERVER_CGI
	serv = soup_server_cgi ();
#else
	serv = StockQuote_get_server ();
#endif

	g_print ("Starting server on port %d\n", soup_server_get_port (serv));
	g_print ("Waiting for requests...\n");

	soup_server_init (serv);
	soup_server_run (serv);
	soup_server_unref (serv);

	return 0;
}
#endif
