/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * server-test.c: Standalone server test.
 *
 * Authors:
 *      Alex Graveley (alex@ximian.com)
 *
 * Copyright (C) 2001, Ximian, Inc.
 */

#include <glib.h>
#include <string.h>

#include <libsoup/soup.h>

gchar payload [] = "Hello World!";

static void 
hello_callback (SoupServerContext *context, 
		SoupMessage       *msg, 
		gpointer           user_data)
{
	soup_message_set_error (msg, SOUP_ERROR_OK);

	msg->response.owner = SOUP_BUFFER_STATIC;
	msg->response.body = payload;
	msg->response.length = strlen (payload);
}

static void
append_header (gchar *key, gchar *value, GString *body)
{
	g_string_sprintfa (body, "%s: %s\n", key, value);
}

static void 
default_callback (SoupServerContext *context, 
		  SoupMessage       *msg, 
		  gpointer           user_data)
{
	GString *body;
	gchar *uri;

	soup_message_set_error_full (msg, SOUP_ERROR_OK, "All Is Well");

	soup_message_add_header (msg->response_headers, 
				 "X-TestMulti", 
				 "I like cheese");

	soup_message_add_header (msg->response_headers, 
				 "X-TestMulti", 
				 "Especially Edam");

	soup_message_add_header (msg->response_headers, 
				 "This-Is-A-Test", 
				 "The rain in spain...");

	body = g_string_new (NULL);

	uri = soup_uri_to_string (soup_context_get_uri (msg->context), TRUE);
	g_string_sprintfa (body, 
			   "Requested Uri: %s\n\n", 
			   uri);
	g_free (uri);

	g_string_sprintfa (body, "Request Headers:\n");
	soup_message_foreach_header (msg->request_headers, 
				     (GHFunc) append_header, 
				     body);

	g_string_sprintfa (body, 
			   "\nRequest Body:\n----\n%s\n----\n", 
			   msg->request.body);

	msg->response.owner = SOUP_BUFFER_SYSTEM_OWNED;
	msg->response.body = body->str;
	msg->response.length = body->len;

	g_string_free (body, FALSE);
}

void
soup_server_init (SoupServer *server)
{
	/*
	 * Register a handler for requests for "/hello"
	 */
	soup_server_register (server, 
			      "/hello", 
			      NULL, 
			      hello_callback, 
			      NULL,
			      NULL);

	/*
	 * Register a handler for all other request paths
	 */
	soup_server_register (server, 
			      NULL, 
			      NULL, 
			      default_callback, 
			      NULL, 
			      NULL);
}
