#include <glib.h>

#include <libsoup/soup.h>
#include <libwsdl/wsdl.h>

static const guchar *xml=
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"<SOAP-ENV:Envelope "
        "SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"" 
        "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">"
  "<SOAP-ENV:Body>"
    "<namesp1:HelloWorldResponse xmlns:namesp1=\"World\">"
      "<message>Hello World</message>"
      "<other>42</other>"
      "<str>"
        "<element2>3.141592658589793</element2>"
        "<element1>Struct element1</element1>"
      "</str>"
      "<str2>"
        "<element1>Struct2 element1</element1>"
        "<element2>"
          "<element1>Wibble</element1>"
          "<element2>2.718281828</element2>"
        "</element2>"
      "</str2>"
      "<list>"
        "<uint32>"
          "1"
        "</uint32>"
      "</list>"
      "<list>"
        "<uint32>"
          "2"
        "</uint32>"
      "</list>"
      "<list>"
        "<uint32>"
          "3"
        "</uint32>"
      "</list>"
      "<slist>"
        "<string>"
          "1"
        "</string>"
      "</slist>"
      "<slist>"
        "<string>"
          "2"
        "</string>"
      "</slist>"
      "<slist>"
        "<string>"
          "3"
        "</string>"
      "</slist>"
      "<strlist>"
        "<structType>"
          "<element2>1.234</element2>"
          "<element1>Struct element1</element1>"
        "</structType>"
      "</strlist>"
      "<strlist>"
        "<structType>"
          "<element2>9.876</element2>"
          "<element1>Another element1</element1>"
        "</structType>"
      "</strlist>"
    "</namesp1:HelloWorldResponse>"
  "</SOAP-ENV:Body>"
"</SOAP-ENV:Envelope>";

typedef struct {
	guchar *element1;
	gfloat element2;
} structType;

typedef struct {
	guchar *element1;
	structType *element2;
} struct2Type;

static void 
structType_free (gpointer data)
{
	structType *item = (structType *) data;

	g_print ("Freeing structType at %p\n", item);
	g_print ("item->element1 [%s]\n", item->element1);
	g_print ("item->element2 %f\n", item->element2);
	
	if (item->element1)
		g_free (item->element1);
	
	g_free(item);
}

static void 
struct2Type_free (gpointer data)
{
	struct2Type *item = (struct2Type *) data;

	g_print ("Freeing struct2Type at %p\n", item);
	g_print ("item->element1 [%s]\n", item->element1);

	if (item->element2) {
		g_print ("item->element2->element1 %s\n",
			 item->element2->element1);
		g_print ("item->element2->element2 %f\n",
			 item->element2->element2);
	}
	
	if (item->element1)
		g_free(item->element1);

	if (item->element2)
		structType_free(item->element2);
	
	g_free(item);
}


static const wsdl_typecode *wsdl_subtypes_array_0 [] = {
	&WSDL_TC_glib_string_struct,
};

const wsdl_typecode WSDL_TC_stringType_struct = {
	WSDL_TK_GLIB_ELEMENT, "stringType", "", "", 
	FALSE, 1, NULL, wsdl_subtypes_array_0, NULL
};

static const guchar *wsdl_subnames_array_1 [] = {
	"element1",
	"element2",
};
static const wsdl_typecode *wsdl_subtypes_array_2 [] = {
	&WSDL_TC_glib_string_struct,
	&WSDL_TC_glib_float_struct,
};
const wsdl_typecode WSDL_TC_structType_struct = {
	WSDL_TK_GLIB_STRUCT, "structType", "", "", 
	FALSE, 2, wsdl_subnames_array_1, wsdl_subtypes_array_2, structType_free
};

static const guchar *wsdl_subnames_array_3 [] = {
	"element1",
	"element2",
};
static const wsdl_typecode *wsdl_subtypes_array_4 [] = {
	&WSDL_TC_glib_string_struct,
	&WSDL_TC_structType_struct,
};
const wsdl_typecode WSDL_TC_struct2Type_struct = {
	WSDL_TK_GLIB_STRUCT, "struct2Type", "", "", 
	FALSE, 2, wsdl_subnames_array_3, wsdl_subtypes_array_4, struct2Type_free
};

static const wsdl_typecode *wsdl_subtypes_array_5 [] = {
	&WSDL_TC_glib_uint32_struct,
};
const wsdl_typecode WSDL_TC_listType_struct = {
	WSDL_TK_GLIB_LIST, "listType", "", "", 
	FALSE, 1, NULL, wsdl_subtypes_array_5, NULL};

static const wsdl_typecode *wsdl_subtypes_array_6 [] = {
	&WSDL_TC_glib_string_struct,
};
const wsdl_typecode WSDL_TC_slistType_struct = {
	WSDL_TK_GLIB_LIST, "slistType", "", "", 
	FALSE, 1, NULL, wsdl_subtypes_array_6, NULL
};

static const wsdl_typecode *wsdl_subtypes_array_7 [] = {
	&WSDL_TC_structType_struct,
};
const wsdl_typecode WSDL_TC_strlistType_struct = {
	WSDL_TK_GLIB_LIST, "strlistType", "", "", 
	FALSE, 1, NULL, wsdl_subtypes_array_7, NULL
};

int 
main (int argc, char **argv)
{
	guchar *mess;
	guint32 other;
	structType *str;
	struct2Type *str2;
	GSList *list, *slist, *strlist, *iter;
	wsdl_param params []= {
		{"message", &mess, &WSDL_TC_stringType_struct},
		{"other",   &other, &WSDL_TC_glib_int32_struct},
		{"str",     &str, &WSDL_TC_structType_struct},
		{"str2",    &str2, &WSDL_TC_struct2Type_struct},
		{"list",    &list, &WSDL_TC_listType_struct},
		{"slist",   &slist, &WSDL_TC_slistType_struct},
		{"strlist", &strlist, &WSDL_TC_strlistType_struct},
		{NULL, NULL, NULL},
	};
	SoupDataBuffer buf;
	SoupEnv *env;

	g_print ("sizeof int32 is %d\n", sizeof (gint32));
	wsdl_typecode_alloc (&WSDL_TC_glib_int32_struct);

	g_print ("sizeof float is %d\n", sizeof (gfloat));
	wsdl_typecode_alloc (&WSDL_TC_glib_float_struct);

	g_print ("sizeof structType is %d\n", sizeof (structType));
	wsdl_typecode_alloc (&WSDL_TC_structType_struct);

	g_print ("sizeof struct2Type is %d\n", sizeof (struct2Type));
	wsdl_typecode_alloc (&WSDL_TC_struct2Type_struct);
	
	env = soup_env_new ();
	wsdl_soap_parse (xml, "HelloWorldResponse", params, env, 0);

	g_print ("mess is [%s]\n", mess);
	g_print ("other is %d\n", other);
	g_print ("str.element1 is [%s]\n", str->element1);
	g_print ("str.element2 is %f\n", str->element2);
	g_print ("str2.element1 is [%s]\n", str2->element1);
	g_print ("str2.element2.element1 is [%s]\n", str2->element2->element1);
	g_print ("str2.element2.element2 is %f\n", str2->element2->element2);

	for (iter = list; iter; iter=iter->next) {
		guint32 *data = (guint32 *) iter->data;
		
		g_print ("list is %d\n", *data);
	}

	for (iter = slist; iter; iter=iter->next) {
		guchar *data = (guchar *) iter->data;
		
		g_print ("slist is [%s]\n", data);
	}

	for (iter = strlist; iter; iter=iter->next) {
		structType *data = (structType *) iter->data;
		
		g_print ("strlist.str.element1 is [%s]\n", data->element1);
		g_print ("strlist.str.element2 is %f\n", data->element2);
	}
	
	wsdl_soap_marshal ("HelloWorldResponse", 
			   "namesp1", 
			   "World", 
			   params,
			   &buf, 
			   env, 
			   0);
	g_print ("Marshalled %d bytes: \n", buf.length);
	g_print (buf.body);
	g_print ("\n");
	
	wsdl_soap_free (params);
	
	exit (0);
}
