/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * wsdl.h: Runtime WSDL support
 *
 * Authors:
 *	Dick Porter (dick@ximian.com)
 *
 * Copyright (C) 2001, Ximian, Inc.
 */

#ifndef _WSDL_H_
#define _WSDL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <libwsdl/wsdl-typecodes.h>
#include <libwsdl/wsdl-typecodes-c.h>
#include <libwsdl/wsdl-schema.h>
#include <libwsdl/wsdl-schema-glib.h>
#include <libwsdl/wsdl-soap-parse.h>
#include <libwsdl/wsdl-soap-marshal.h>
#include <libwsdl/wsdl-soap-memory.h>
#include <libwsdl/wsdl-param.h>

#ifdef __cplusplus
}
#endif

#endif /* _WSDL_H_ */
