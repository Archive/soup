/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * wsdl-trace.h: Emit skeleton code for servers
 *
 * Authors:
 *	Dick Porter (dick@ximian.com)
 *
 * Copyright (C) 2001, Ximian, Inc.
 */

#ifndef _WSDL_TRACE_H_
#define _WSDL_TRACE_H_

#define WSDL_LOG_DOMAIN_STUBS   1<<0
#define WSDL_LOG_DOMAIN_SKELS   1<<1
#define WSDL_LOG_DOMAIN_HEADERS 1<<2
#define WSDL_LOG_DOMAIN_COMMON  1<<3
#define WSDL_LOG_DOMAIN_PARSER  1<<4
#define WSDL_LOG_DOMAIN_THREAD  1<<5
#define WSDL_LOG_DOMAIN_MAIN    1<<6

void wsdl_parse_debug_domain_string (const guchar *string);
void wsdl_parse_debug_level_string  (const guchar *string);
void wsdl_debug                     (guint         domain,
				     guint         level,
				     const guchar *format,
				     ...);

#endif /* _WSDL_SKELS_H_ */
