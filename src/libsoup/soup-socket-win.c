/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * soup-socket-win.c: Windows socket networking code.
 *
 * Authors:
 *      David Helder  (dhelder@umich.edu)
 *      Alex Graveley (alex@ximian.com)
 *
 * Original code compliments of David Helder's GNET Networking Library, and is
 * Copyright (C) 2000  David Helder & Andrew Lanoix.
 *
 * All else Copyright (C) 2000, Ximian, Inc.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <fcntl.h>
#include <glib.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "soup-private.h"
#include "soup-socket.h"

#include <windows.h>
#include <winbase.h>
#include <winuser.h>
#include <io.h>

WNDCLASSEX soupWndClass;
HWND  soup_hWnd;
guint soup_io_watch_ID;
GIOChannel *soup_iochannel;

GHashTable *soup_hash;
HANDLE soup_Mutex;
HANDLE soup_hostent_Mutex;

#define IA_NEW_MSG 100		/* soup_address_new */
#define GET_NAME_MSG 101	/* soup_address_get_name */

#define SOUP_ANY_IO_CONDITION  (G_IO_IN | G_IO_OUT | G_IO_PRI | \
                                G_IO_ERR | G_IO_HUP | G_IO_NVAL)

typedef struct {
	SoupAddress       ia;
	SoupAddressNewFn  func;
	gpointer          data;

	int               WSAhandle;
	char              hostentBuffer [MAXGETHOSTSTRUCT];
	int               errorcode;
} SoupAddressState;

typedef struct {
	SoupAddress          *ia;
	SoupAddressGetNameFn  func;
	gpointer              data;

	int                   WSAhandle;
	char                  hostentBuffer [MAXGETHOSTSTRUCT];
	int                   errorcode;
} SoupAddressReverseState;

typedef struct {
	gint             sockfd;
	SoupAddress     *addr;
	SoupSocketNewFn  func;
	gpointer         data;
	gint             flags;
	guint            connect_watch;

	gint             errorcode;
} SoupSocketState;

gboolean
soup_gethostbyname(const char*         hostname,
		   struct sockaddr_in* sa,
		   gchar**             nicename)
{
	gboolean rv = FALSE;
	struct hostent *result;

	WaitForSingleObject (soup_hostent_Mutex, INFINITE);
	result = gethostbyname (hostname);

	if (result != NULL) {
		if (sa) {
			sa->sin_family = result->h_addrtype;
			memcpy (&sa->sin_addr,
				result->h_addr_list [0],
				result->h_length);
		}

		if (nicename && result->h_name)
			*nicename = g_strdup(result->h_name);

		ReleaseMutex(soup_hostent_Mutex);
		rv = TRUE;
	}

	return rv;
}

gchar *
soup_gethostbyaddr (const char* addr, size_t length, int type)
{
	gchar* rv = NULL;
	struct hostent* he;

	WaitForSingleObject (soup_hostent_Mutex, INFINITE);
	he = gethostbyaddr (addr, length, type);
	if (he != NULL && he->h_name != NULL)
		rv = g_strdup (he->h_name);
	ReleaseMutex (soup_hostent_Mutex);

	return rv;
}

static gboolean
soup_address_new_cb (GIOChannel* iochannel,
		     GIOCondition condition,
		     gpointer data)
{
	SoupAddressState* state = (SoupAddressState*) data;
	SoupAddressNewFn cb_func = state->func;
	gpointer cb_data = state->data;
	struct hostent *result;
	struct sockaddr_in *sa_in;

	if (state->errorcode) {
		(*state->func) (&state->ia,
				SOUP_ADDRESS_STATUS_ERROR,
				state->data);
		g_free (state);
		return FALSE;
	}

	result = (struct hostent*) state->hostentBuffer;

	sa_in = (struct sockaddr_in*) &state->ia.sa;
	memcpy (&sa_in->sin_addr, result->h_addr_list [0], result->h_length);

	state->ia.name = g_strdup (result->h_name);

	state = g_realloc (state, sizeof (SoupAddress));

	(*cb_func) (&state->ia, SOUP_ADDRESS_STATUS_OK, cb_data);

	return FALSE;
}

SoupAddressNewId
soup_address_new (const gchar* name,
		  const gint port,
		  SoupAddressNewFn func,
		  gpointer data)
{
	struct in_addr inaddr;
	struct sockaddr_in* sa_in;
	SoupAddressState* state;

	g_return_val_if_fail(name != NULL, NULL);
	g_return_val_if_fail(func != NULL, NULL);

	/* Try to read the name as if were dotted decimal */

	inaddr.s_addr = inet_addr(name);
	if (inaddr.s_addr != INADDR_NONE) {
		SoupAddress* ia = NULL;
		struct sockaddr_in* sa_in;

		ia = g_new0(SoupAddress, 1);
		ia->ref_count = 1;

		sa_in = (struct sockaddr_in*) &ia->sa;
		sa_in->sin_family = AF_INET;
		sa_in->sin_port = g_htons (port);
		memcpy (&sa_in->sin_addr,
			(char*) &inaddr,
			sizeof (struct in_addr));

		(*func) (ia, SOUP_ADDRESS_STATUS_OK, data);
		return NULL;
	}

	/* Create a structure for the call back */
	state = g_new0 (SoupAddressState, 1);
	state->ia.name = g_strdup (name);
	state->ia.ref_count = 1;
	state->func = func;
	state->data = data;

	sa_in = (struct sockaddr_in*) &state->ia.sa;
	sa_in->sin_family = AF_INET;
	sa_in->sin_port = g_htons (port);

	state->WSAhandle = (int)
		WSAAsyncGetHostByName (soup_hWnd,
				       IA_NEW_MSG,
				       name,
				       state->hostentBuffer,
				       sizeof (state->hostentBuffer));

	if (!state->WSAhandle) {
		g_free (state);
		(*func) (NULL, SOUP_ADDRESS_STATUS_ERROR, data);
		return NULL;
	}

	/*get a lock and insert the state into the hash */
	WaitForSingleObject (soup_Mutex, INFINITE);
	g_hash_table_insert (soup_hash,
			     (gpointer) state->WSAhandle,
			     (gpointer) state);
	ReleaseMutex (soup_Mutex);

	return state;
}

void
soup_address_new_cancel (SoupAddressNewId id)
{
	SoupAddressState* state = (SoupAddressState*) id;

	g_return_if_fail(state != NULL);

	WSACancelAsyncRequest ((HANDLE)state->WSAhandle);

	/*get a lock and remove the hash entry */
	WaitForSingleObject (soup_Mutex, INFINITE);
	g_hash_table_remove (soup_hash, (gpointer) state->WSAhandle);
	ReleaseMutex (soup_Mutex);
	g_free (state);
}

void
soup_address_unref (SoupAddress* ia)
{
	g_return_if_fail (ia != NULL);

	--ia->ref_count;

	if (ia->ref_count == 0) {
		if (ia->name != NULL)
			g_free (ia->name);

		g_free (ia);
	}
}

static gboolean
soup_address_get_name_cb (GIOChannel* iochannel,
			  GIOCondition condition,
			  gpointer data)
{
	SoupAddressReverseState* state = (SoupAddressReverseState*) data;
	struct hostent* result;

	if (state->errorcode) {
		(*state->func) (state->ia,
				SOUP_ADDRESS_STATUS_ERROR,
				NULL,
				state->data);
		g_free (state);
		return FALSE;
	}

	result = (struct hostent*) state->hostentBuffer;

	state->ia->name = g_strdup (result->h_name);

	(*state->func) (state->ia,
			SOUP_ADDRESS_STATUS_OK,
			state->ia->name,
			state->data);

	g_free (state);
	return FALSE;
}

SoupAddressGetNameId
soup_address_get_name (SoupAddress* ia,
		       SoupAddressGetNameFn func,
		       gpointer data)
{
	SoupAddressReverseState* state;
	struct sockaddr_in* sa_in;

	g_return_val_if_fail(ia != NULL, NULL);
	g_return_val_if_fail(func != NULL, NULL);

	/* If we already know the name, just copy that */
	if (ia->name != NULL) {
		(func) (ia,
			SOUP_ADDRESS_STATUS_OK,
			g_strdup (ia->name),
			data);
	}

	/* Create a structure for the call back */
	state = g_new0 (SoupAddressReverseState, 1);
	state->ia = ia;
	state->func = func;
	state->data = data;

	sa_in = (struct sockaddr_in*) &ia->sa;

	state->WSAhandle = (int)
		WSAAsyncGetHostByAddr (soup_hWnd, GET_NAME_MSG,
				       (const char*) &sa_in->sin_addr,
				       (int) (sizeof (&sa_in->sin_addr)),
				       (int) &sa_in->sin_family,
				       state->hostentBuffer,
				       sizeof (state->hostentBuffer));

	if (!state->WSAhandle) {
		g_free (state);
		(func) (ia, SOUP_ADDRESS_STATUS_ERROR, NULL, data);
		return NULL;
	}

	/*get a lock and insert the state into the hash */
	WaitForSingleObject (soup_Mutex, INFINITE);
	g_hash_table_insert (soup_hash,
			     (gpointer) state->WSAhandle,
			     (gpointer) state);
	ReleaseMutex (soup_Mutex);

	return state;
}

void
soup_address_get_name_cancel (SoupAddressGetNameId id)
{
	SoupAddressReverseState* state;
	state = (SoupAddressReverseState*) id;

	g_return_if_fail(state != NULL);

	soup_address_unref (state->ia);
	WSACancelAsyncRequest ((HANDLE) state->WSAhandle);

	/*get a lock and remove the hash entry */
	WaitForSingleObject (soup_Mutex, INFINITE);
	g_hash_table_remove (soup_hash, (gpointer) state->WSAhandle);
	ReleaseMutex (soup_Mutex);

	g_free (state);
}

gchar*
soup_address_gethostname (void)
{
	gchar* name = NULL;
	int error = 0;

	name = g_new0 (char, 256);
	error = gethostname (name, 256);
	if (error) {
		g_free(name);
		return NULL;
	}

	return name;
}

SoupAddress *
soup_address_get_cached (const gchar       *name, 
			 const gint         port)
{
	return NULL;
}

static gboolean
soup_socket_new_cb (GIOChannel* iochannel,
		    GIOCondition condition,
		    gpointer data)
{
	SoupSocketState* state = (SoupSocketState*) data;
	SoupSocket *s;

	/* Remove the watch now in case we don't return immediately */
	g_source_remove (state->connect_watch);

	if (condition & ~(G_IO_IN | G_IO_OUT)) goto _ERROR;

	s = g_new0 (SoupSocket, 1);
	s->ref_count = 1;
	s->sockfd = state->sockfd;
	s->addr = state->addr;

	(*state->func) (s, SOUP_SOCKET_NEW_STATUS_OK, state->data);
	g_free (state);
	return FALSE;

 _ERROR:
	soup_address_unref (state->addr);
	(*state->func) ((SoupSocket *) NULL,
			SOUP_SOCKET_NEW_STATUS_ERROR,
			state->data);
	g_free (state);

	return FALSE;
}

SoupSocketNewId
soup_socket_new (SoupAddress     *addr,
		 SoupSocketNewFn  func,
		 gpointer         data)
{
	gint sockfd;
	gint status;
	SoupSocketState* state = (SoupSocketState*) data;
	u_long arg = 1;
	GIOChannel *chan;

	g_return_val_if_fail (addr != NULL, NULL);
	g_return_val_if_fail (func != NULL, NULL);

	/* Create socket */
	sockfd = socket (AF_INET, SOCK_STREAM, 0);
	if (sockfd == INVALID_SOCKET) {
		(func) (NULL, SOUP_SOCKET_NEW_STATUS_ERROR, data);
		return NULL;
	}

	/* Set non-blocking mode */
	ioctlsocket(sockfd, FIONBIO, &arg);

	status = connect (sockfd, &addr->sa, sizeof(addr->sa));
	/* Returning an error is ok, unless.. */
	if (status == SOCKET_ERROR) {
		status = WSAGetLastError();
		if (status != WSAEWOULDBLOCK) {
			(func) (NULL, SOUP_SOCKET_NEW_STATUS_ERROR, data);
			return NULL;
		}
	}

	soup_address_ref (addr);

	if (status != SOCKET_ERROR) {
		SoupSocket *s = g_new0 (SoupSocket, 1);
		s->ref_count = 1;
		s->sockfd = sockfd;
		s->addr = addr;

		(*func) (s, SOUP_SOCKET_NEW_STATUS_OK, data);
		return NULL;
	}

	chan = g_io_channel_win32_new_socket (sockfd),

	/* Wait for the connection */
	state = g_new0 (SoupSocketState, 1);
	state->addr = addr;
	state->func = func;
	state->data = data;
	state->sockfd = sockfd;
	state->connect_watch = g_io_add_watch (chan,
					       SOUP_ANY_IO_CONDITION,
					       soup_socket_new_cb, 
					       state);

	g_io_channel_unref (chan);

	return state;
}

void
soup_socket_new_cancel (SoupSocketNewId id)
{
	SoupSocketState* state = (SoupSocketState*) id;

	g_source_remove (state->connect_watch);
	soup_address_unref (state->addr);
	g_free (state);
}

SoupSocket *
soup_socket_server_accept (SoupSocket *socket)
{
	gint sockfd;
	struct sockaddr sa;
	gint n;
	fd_set fdset;
	SoupSocket* s;
	u_long arg;

	g_return_val_if_fail (socket != NULL, NULL);

	FD_ZERO (&fdset);
	FD_SET ((unsigned)socket->sockfd, &fdset);

	if (select (socket->sockfd + 1, &fdset, NULL, NULL, NULL) == -1) {
		return NULL;
	}

	/* make sure the socket is in blocking mode */

	arg = 0;
	if (ioctlsocket (socket->sockfd, FIONBIO, &arg))
		return NULL;

	sockfd = accept (socket->sockfd, &sa, NULL);
	/* if it fails, looping isn't going to help */

	if (sockfd == INVALID_SOCKET) {
		return NULL;
	}

	s = g_new0 (SoupSocket, 1);
	s->ref_count = 1;
	s->sockfd = sockfd;

	s->addr = g_new0 (SoupAddress, 1);
	s->addr->ref_count = 1;
	memcpy (&s->addr->sa, &sa, sizeof (s->addr->sa));

	return s;
}

SoupSocket *
soup_socket_server_try_accept (SoupSocket *socket)
{
	gint sockfd;
	struct sockaddr sa;

	fd_set fdset;
	SoupSocket* s;
	u_long arg;

	g_return_val_if_fail (socket != NULL, NULL);
	FD_ZERO (&fdset);
	FD_SET ((unsigned)socket->sockfd, &fdset);

	if (select (socket->sockfd + 1, &fdset, NULL, NULL, NULL) == -1) {
		return NULL;
	}
	/* make sure the socket is in non-blocking mode */

	arg = 1;
	if (ioctlsocket (socket->sockfd, FIONBIO, &arg))
		return NULL;

	sockfd = accept (socket->sockfd, &sa, NULL);
	/* if it fails, looping isn't going to help */

	if (sockfd == INVALID_SOCKET) {
		return NULL;
	}

	s = g_new0 (SoupSocket, 1);
	s->ref_count = 1;
	s->sockfd = sockfd;

	s->addr = g_new0 (SoupAddress, 1);
	s->addr->ref_count = 1;
	memcpy (&s->addr->sa, &sa, sizeof (s->addr->sa));

	return s;
}

int
soup_MainCallBack (GIOChannel   *iochannel,
		   GIOCondition  condition,
		   void         *nodata)
{
	MSG msg;

	gpointer data;
	SoupAddressState *IAstate;
	SoupAddressReverseState *IARstate;

	/*Take the msg off the message queue */
	PeekMessage (&msg, soup_hWnd, 0, 0, PM_REMOVE);

	switch (msg.message) {
	case IA_NEW_MSG:
		WaitForSingleObject (soup_Mutex, INFINITE);
		data = g_hash_table_lookup (soup_hash, (gpointer) msg.wParam);
		g_hash_table_remove (soup_hash, (gpointer) msg.wParam);
		ReleaseMutex (soup_Mutex);

		IAstate = (SoupAddressState*) data;
		/* NULL if OK */
		IAstate->errorcode = WSAGETASYNCERROR(msg.lParam);

		/* Now call the callback function */
		soup_address_new_cb (NULL, G_IO_IN, (gpointer) IAstate);

		break;
	case GET_NAME_MSG:
		WaitForSingleObject (soup_Mutex, INFINITE);
		data = g_hash_table_lookup (soup_hash, (gpointer) msg.wParam);
		g_hash_table_remove (soup_hash, (gpointer) msg.wParam);
		ReleaseMutex (soup_Mutex);

		IARstate = (SoupAddressReverseState*) data;
		/* NULL if OK */
		IARstate->errorcode = WSAGETASYNCERROR(msg.lParam);

		/* Now call the callback function */
		soup_address_get_name_cb (NULL,
					  G_IO_IN,
					  (gpointer) IARstate);
		break;
	}

	return 1;
}

LRESULT CALLBACK
SoupWndProc (HWND hwnd,        /* handle to window */
	     UINT uMsg,        /* message identifier */
	     WPARAM wParam,    /* first message parameter */
	     LPARAM lParam)    /* second message parameter */
{
	switch (uMsg) {
        case WM_CREATE:  /* Initialize the window. */
		return 0;
        case WM_PAINT:   /* Paint the window's client area. */
		return 0;
        case WM_SIZE:    /* Set the size and position of the window. */
		return 0;
        case WM_DESTROY: /* Clean up window-specific data objects. */
		return 0;

        default:
		return DefWindowProc (hwnd, uMsg, wParam, lParam);
	}
}

BOOL WINAPI
DllMain (HINSTANCE hinstDLL,  /* handle to DLL module */
	 DWORD fdwReason,     /* reason for calling functionm */
	 LPVOID lpvReserved   /* reserved */)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	switch(fdwReason) {
	case DLL_PROCESS_ATTACH:
		/* The DLL is being mapped into process's address space */
		/* Do any required initialization on a per application basis,
		   return FALSE if failed */
		wVersionRequested = MAKEWORD (2, 0);

		err = WSAStartup (wVersionRequested, &wsaData);
		if (err != 0) {
			/* Tell the user that we could not find a usable
			   WinSock DLL.  */
			return FALSE;
		}

		/* Confirm that the WinSock DLL supports 2.0.*/
		/* Note that if the DLL supports versions greater    */
		/* than 2.0 in addition to 2.0, it will still return */
		/* 2.0 in wVersion since that is the version we      */
		/* requested.                                        */

		if (LOBYTE(wsaData.wVersion) != 2 ||
		    HIBYTE(wsaData.wVersion) != 0) {
			/* Tell the user that we could not find a usable */
			/* WinSock DLL.                                  */
			WSACleanup ();
			return FALSE;
		}

		/* The WinSock DLL is acceptable. Proceed. */

		/* Setup and register a windows class that we use for out
                   GIOchannel */
		soupWndClass.cbSize = sizeof (WNDCLASSEX);
		soupWndClass.style = CS_SAVEBITS;
		soupWndClass.lpfnWndProc = (WNDPROC) SoupWndProc;
		soupWndClass.cbClsExtra = 0;
		soupWndClass.cbWndExtra = 0;
		soupWndClass.hInstance = hinstDLL;
		soupWndClass.hIcon = NULL;
		soupWndClass.hCursor = NULL;
		soupWndClass.hbrBackground = NULL;
		soupWndClass.lpszMenuName = NULL;
		soupWndClass.lpszClassName = "Soup";
		soupWndClass.hIconSm = NULL;

		if (!RegisterClassEx (&soupWndClass)) return FALSE;

		soup_hWnd  = CreateWindowEx (0,
					     "Soup",
					     "none",
					     WS_OVERLAPPEDWINDOW,
					     CW_USEDEFAULT,
					     CW_USEDEFAULT,
					     CW_USEDEFAULT,
					     CW_USEDEFAULT,
					     (HWND) NULL,
					     (HMENU) NULL,
					     hinstDLL,
					     (LPVOID) NULL);

		if (!soup_hWnd) return FALSE;

		soup_iochannel =
			g_io_channel_win32_new_messages (
				(unsigned int) soup_hWnd);

		/* Add a watch */
		soup_io_watch_ID =
			g_io_add_watch (soup_iochannel,
					G_IO_IN|G_IO_ERR|G_IO_HUP|G_IO_NVAL,
					soup_MainCallBack,
					NULL);

		soup_hash = g_hash_table_new (NULL, NULL);

		soup_Mutex = CreateMutex (NULL, FALSE, "soup_Mutex");
		if (soup_Mutex == NULL) return FALSE;

		soup_hostent_Mutex = CreateMutex (NULL,
						  FALSE,
						  "soup_hostent_Mutex");
		if (soup_hostent_Mutex == NULL) return FALSE;

		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		/* The DLL unmapped from process's address space. Do necessary
                   cleanup */
		g_source_remove (soup_io_watch_ID);
		g_free (soup_iochannel);
		DestroyWindow (soup_hWnd);

		WSACleanup ();

		break;
	}

	return TRUE;
}
