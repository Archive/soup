/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * soup.h: Asyncronous Callback-based SOAP Request Queue.
 *
 * Authors:
 *      Alex Graveley (alex@helixcode.com)
 *
 * Copyright (C) 2000, Helix Code, Inc.
 */

#ifndef SOUP_H
#define SOUP_H 1

#ifdef __cplusplus
extern "C" {
#endif

#include <libsoup/soup-context.h>
#include <libsoup/soup-dav.h>
#include <libsoup/soup-dav-server.h>
#include <libsoup/soup-message.h>
#include <libsoup/soup-misc.h>
#include <libsoup/soup-fault.h>
#include <libsoup/soup-env.h>
#include <libsoup/soup-parser.h>
#include <libsoup/soup-serializer.h>
#include <libsoup/soup-socket.h>
#include <libsoup/soup-uri.h>

#ifdef __cplusplus
}
#endif

#endif /*SOUP_H*/
