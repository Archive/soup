# Microsoft Developer Studio Project File - Name="libsoup" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=libsoup - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libsoup.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libsoup.mak" CFG="libsoup - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libsoup - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libsoup - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libsoup - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "SOUP_CORE_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "SOUP_CORE_EXPORTS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 iconv-1.3.lib glib-1.3.lib gmodule-1.3.lib libxml2.lib oldnames.lib ws2_32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386

!ELSEIF  "$(CFG)" == "libsoup - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "libsoup___Win32_Debug"
# PROP BASE Intermediate_Dir "libsoup___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "libsoup___Win32_Debug"
# PROP Intermediate_Dir "libsoup___Win32_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "SOUP_CORE_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "_WINDOWS" /D "_USRDLL" /D "SOUP_CORE_EXPORTS" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "SOUP_WIN32" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 iconv-1.3.lib glib-1.3.lib gmodule-1.3.lib libxml2.lib oldnames.lib ws2_32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /nodefaultlib:"libcd" /pdbtype:sept
# SUBTRACT LINK32 /nodefaultlib

!ENDIF 

# Begin Target

# Name "libsoup - Win32 Release"
# Name "libsoup - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\libsoup.def
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\md5-utils.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-auth.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-context.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-dav-server.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-dav.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-env.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-error.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-fault.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-headers.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-message.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-method.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-misc.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-nss.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-ntlm.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-parser.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-queue.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-serializer.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-server.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-socket-win.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-socket.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-socks.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-ssl.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-transfer.c"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-uri.c"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE="..\src\libsoup\md5-utils.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-auth.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-context.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-dav-server.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-dav.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-env.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-error.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-fault.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-headers.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-message.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-method.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-misc.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-nss.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-ntlm.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-parser.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-private.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-serializer.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-server-auth.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-server.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-socket.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-socks.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-ssl.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-transfer.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup-uri.h"
# End Source File
# Begin Source File

SOURCE="..\src\libsoup\soup.h"
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
